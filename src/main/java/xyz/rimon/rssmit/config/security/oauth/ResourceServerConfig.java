package xyz.rimon.rssmit.config.security.oauth;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import xyz.rimon.rssmit.domains.Role;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
//                .antMatcher("/api/v1/**")
                .authorizeRequests()
                .antMatchers(
                        "/",
                        "/register/**",
                        "/dev/register",
                        "/dev/client/credentials",
                        "/api/v1/users/create",
                        "/login**",
                        "/oauth/token**",
                        "/swagger-resources/**",
                        "/v2/api-docs",
                        "/resetPassword/**",
                        "/checkTokenValidity"
                )
                .permitAll()
                .antMatchers(
                        "/dev/**",
                        "/api/v1/users/**",
                        "/api/v1/rshared/**",
                        "/api/v1/transactions/create",
                        "/api/v1/transactions/byClient/*",
                        "/api/v1/apps/**"
                )
                .hasAnyAuthority(Role.StringRole.ROLE_DEVELOPER, Role.StringRole.ROLE_ADMIN)
                .antMatchers(
                        "/api/v1/admin/**"
                )
                .hasAuthority(Role.StringRole.ROLE_ADMIN)
                .anyRequest()
                .authenticated()
                .and().logout().logoutSuccessUrl("/").permitAll();
    }
}
