package xyz.rimon.rssmit.commons.utils;

public class Validator {
    private Validator() {
    }

    public static boolean nullOrZero(Object object) {
        return object == null || object.equals(0);
    }
}
