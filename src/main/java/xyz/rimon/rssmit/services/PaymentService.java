package xyz.rimon.rssmit.services;

import org.springframework.data.domain.Page;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.PaymentRequest;
import xyz.rimon.rssmit.domains.Transactions;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.exceptions.exists.UserAlreadyPaidException;
import xyz.rimon.rssmit.exceptions.invalid.InvalidException;
import xyz.rimon.rssmit.exceptions.invalid.PaymentRequstInvalidException;
import xyz.rimon.rssmit.exceptions.notfound.PaymentRequestNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;

import java.util.Date;

public interface PaymentService {
    Transactions makePayment(Long userId, Long rShareId, Long amount, String trnxId) throws InvalidException;
    Transactions findUsersLastPayment(Long userId);
    Page<Transactions> getPaymentsForUser(Long userId, int page) throws UserNotFoundException;

    Page<PaymentRequest> getPaymentRequestsForUser(Long userId, int page);

    PaymentRequest createPaymentRequest(PaymentRequest request, User user) throws UserAlreadyPaidException, PaymentRequstInvalidException;

    Page<PaymentRequest> getPaymentRequestsForClient(String clientId, boolean isPaid, int page) throws UserNotFoundException;

    PaymentRequest resolveRequest(Long reqId, Long amount, String trnxId, User currentUser) throws InvalidException, PaymentRequestNotFoundException;

    Long getTotalNumberOfPaymentRequests(OAuth2Client client);

    boolean isUserAlreadyPaid(Long userId, Date fromDate, Date toDate);
}
