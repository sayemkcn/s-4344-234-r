package xyz.rimon.rssmit.services;

import org.springframework.data.domain.Page;
import xyz.rimon.rssmit.domains.Event;
import xyz.rimon.rssmit.domains.EventStatistics;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.exceptions.nullpointers.NullEventException;

import java.util.Date;
import java.util.List;

public interface EventService {
    Event save(Event event) throws NullEventException;
    List<Event> save(List<Event> eventList) throws NullEventException;
    List<Event> save(List<Event> eventList, User user) throws NullEventException, UserNotFoundException;
    Page<Event> getEvents(User user, int page, int size);
    Page<Event> getEvents(int page, int size);
    Page<Event> getEventsByClient(OAuth2Client client,int page,int size);
    EventStatistics getStats(OAuth2Client client, String period);

    Long getUserClientEventsWeightSumInAPeriod(User user, Date fromDate, Date toDate);
    Long getUserEventsWeightSumInAPeriod(User user, Date fromDate, Date toDate);
}
