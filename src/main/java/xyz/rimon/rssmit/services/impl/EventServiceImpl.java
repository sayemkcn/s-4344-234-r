package xyz.rimon.rssmit.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import xyz.rimon.rssmit.commons.PageAttr;
import xyz.rimon.rssmit.commons.utils.DateUtil;
import xyz.rimon.rssmit.domains.Event;
import xyz.rimon.rssmit.domains.EventStatistics;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.exceptions.nullpointers.NullEventException;
import xyz.rimon.rssmit.repository.EventRepository;
import xyz.rimon.rssmit.services.EventService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EventServiceImpl implements EventService {
    private final EventRepository eventRepo;

    @Autowired
    public EventServiceImpl(EventRepository eventRepo) {
        this.eventRepo = eventRepo;
    }

    @Override
    public Event save(Event event) throws NullEventException {
        if (event == null) throw new NullEventException("Event can not be null");
        if (event.getId() != null) {
            Event exEvent = this.eventRepo.findOne(event.getId());
            event.setCreated(exEvent.getCreated());
        }
        return this.eventRepo.save(event);
    }

    @Override
    public List<Event> save(List<Event> eventList) throws NullEventException {
        if (eventList == null || eventList.isEmpty())
            throw new NullEventException("Can not save empty or null event list");
        return this.eventRepo.save(eventList);
    }

    @Override
    public List<Event> save(List<Event> eventList, User user) throws NullEventException, UserNotFoundException {
        for (Event e : eventList) {
            e.setUser(user);
            e.setId(null);
        }
        return this.save(eventList);
    }

    @Override
    public Page<Event> getEvents(User user, int page, int size) {
        if (user == null) throw new IllegalArgumentException("User can not be null!");
        return this.eventRepo.findByUser(user, new PageRequest(page, size, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }

    @Override
    public Page<Event> getEvents(int page, int size) {
        return this.eventRepo.findAll(new PageRequest(page, size, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }

    @Override
    public Page<Event> getEventsByClient(OAuth2Client client, int page, int size) {
        return this.eventRepo.findByUserClient(client, new PageRequest(page, size, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }

    @Override
    public EventStatistics getStats(OAuth2Client client, String period) {
        DateUtil.Period p = DateUtil.getPeriod(period);
        Map<DateUtil.DateRangeType, Calendar> dateRangeMap = DateUtil.buildDateRange(p);
        Date fromDate = dateRangeMap.get(DateUtil.DateRangeType.DATE_FROM).getTime();
        Date toDate = dateRangeMap.get(DateUtil.DateRangeType.DATE_TO).getTime();

        Long totalEvents = this.eventRepo.countByUserClientAndDateBetween(client, fromDate, toDate);
        Long totalWeight = this.eventRepo.sumWeightByUserClientAndDateBetween(client.getId(), fromDate, toDate);
        return new EventStatistics(totalEvents, totalWeight);
    }

    @Override
    public Long getUserClientEventsWeightSumInAPeriod(User user, Date fromDate, Date toDate) {
        return this.eventRepo.sumWeightByUserClientAndDateBetween(user.getClient().getId(),fromDate,toDate);
    }

    @Override
    public Long getUserEventsWeightSumInAPeriod(User user, Date fromDate, Date toDate) {
        return this.eventRepo.sumWeightByUserAndDateBetween(user.getId(),fromDate,toDate);
    }


}
