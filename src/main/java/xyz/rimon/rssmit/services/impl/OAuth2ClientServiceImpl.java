package xyz.rimon.rssmit.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.rimon.rssmit.commons.utils.SessionIdentifierGenerator;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.Role;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;
import xyz.rimon.rssmit.repository.ClientRepository;
import xyz.rimon.rssmit.services.OAuth2ClientService;

@Service
public class OAuth2ClientServiceImpl implements OAuth2ClientService {
    private final ClientRepository clientRepo;

    @Autowired
    public OAuth2ClientServiceImpl(ClientRepository clientRepo) {
        this.clientRepo = clientRepo;
    }

    @Override
    public OAuth2Client save(OAuth2Client client) {
        if (client == null) throw new IllegalArgumentException("Client can not be null");
        SessionIdentifierGenerator sig = new SessionIdentifierGenerator();
        client.setClientId(sig.nextSessionId());
        client.setClientSecret(sig.nextPassword());
        client.setScope("read,write,trust");
        client.setAccessTokenValidity(5000);
        client.setRefreshTokenValidity(1209600000L);
        client.setGrantTypes("password,refresh_token");
        client.setAuthorities(Role.ERole.ROLE_USER.toString());
        return this.clientRepo.save(client);
    }

    @Override
    public OAuth2Client findOne(Long id) throws ClientNotFoundException {
        if (id == null) throw new ClientNotFoundException("Could not find client with id " + id);
        return this.clientRepo.findOne(id);
    }

    @Override
    public OAuth2Client findByClientId(String clientId, String clientSecret) throws ClientNotFoundException {
        if (clientId == null) throw new ClientNotFoundException("Client Id can not be null");
        OAuth2Client client = this.clientRepo.findByClientIdAndClientSecret(clientId,clientSecret);
        if (client == null) throw new ClientNotFoundException("Could not find client with client id " + clientId);
        return client;
    }

    @Override
    public OAuth2Client findByUser(User user) {
        return this.clientRepo.findByUserListIn(user);
    }
}
