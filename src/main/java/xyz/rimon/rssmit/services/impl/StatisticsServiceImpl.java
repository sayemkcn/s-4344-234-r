package xyz.rimon.rssmit.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.rimon.rssmit.domains.EventStatistics;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.Statistics;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;
import xyz.rimon.rssmit.repository.ClientRepository;
import xyz.rimon.rssmit.services.*;

@Service
public class StatisticsServiceImpl implements StatisticsService {
    private EventService eventService;
    private PaymentService paymentService;
    private TransactionService transactionService;
    private UserService userService;
    private RSharedService rSharedService;
    private final ClientRepository clientRepo;

    @Autowired
    public StatisticsServiceImpl(
            EventService eventService,
            PaymentService paymentService,
            TransactionService transactionService,
            UserService userService,
            RSharedService rSharedService,
            ClientRepository clientRepo
    ) {
        this.eventService = eventService;
        this.paymentService = paymentService;
        this.transactionService = transactionService;
        this.userService = userService;
        this.rSharedService = rSharedService;
        this.clientRepo = clientRepo;
    }

    @Override
    public Statistics getStatistics(String clientId, String period) throws ClientNotFoundException {
        Statistics stats = new Statistics();

        OAuth2Client client = this.clientRepo.findByClientId(clientId);
        if (client == null) throw new ClientNotFoundException("Could not find client!");
        EventStatistics eventStats = this.eventService.getStats(client, period);

        stats.setNumberOfEvents(eventStats.getTotalEvents());
        stats.setWeightSum(eventStats.getWeightSum());
        stats.setTotalUsers(this.userService.getTotalNumberOfUsers(client));
        stats.setTotalSharedAmount(this.rSharedService.getTotalSharedAmount(client));
        stats.setTotalLiability(this.userService.getTotalPayableAmount(client));
        stats.setTotalNumberOfTrnx(this.transactionService.getTotalNumberOfTransaction(client));
        stats.setTotalPaymentRequests(this.paymentService.getTotalNumberOfPaymentRequests(client));
        return stats;
    }
}
