package xyz.rimon.rssmit.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import xyz.rimon.rssmit.commons.PageAttr;
import xyz.rimon.rssmit.commons.utils.PasswordUtil;
import xyz.rimon.rssmit.commons.utils.SessionIdentifierGenerator;
import xyz.rimon.rssmit.commons.utils.Validator;
import xyz.rimon.rssmit.domains.*;
import xyz.rimon.rssmit.domains.pojo.Address;
import xyz.rimon.rssmit.exceptions.exists.UserAlreadyExistsException;
import xyz.rimon.rssmit.exceptions.invalid.UserInvalidException;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.exceptions.nullpointers.NullPasswordException;
import xyz.rimon.rssmit.repository.UserRepository;
import xyz.rimon.rssmit.services.*;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@PropertySource("classpath:payments.properties")
public class UserServiceImpl implements UserService {
    private final UserRepository userRepo;
    private final RoleService roleService;
    private final OAuth2ClientService clientService;
    private final TransactionService trnxService;
    private final EventService eventService;
    private final AcValidationTokenService acValidationTokenService;
    private final MailService mailService;

    @Value("${baseUrlApi}")
    private String baseUrlApi;

    @Value("${threshold-limit}")
    private Long thresholdLimit;

    @Value(("${admin.email1}"))
    private String adminEmail1;
    @Value(("${admin.email2}"))
    private String adminEmail2;


    @Autowired
    public UserServiceImpl(UserRepository userRepo,
                           RoleService roleService,
                           OAuth2ClientService clientService,
                           TransactionService trnxService,
                           EventService eventService,
                           AcValidationTokenService acValidationTokenService,
                           MailService mailService) {
        this.userRepo = userRepo;
        this.roleService = roleService;
        this.clientService = clientService;
        this.trnxService = trnxService;
        this.eventService = eventService;
        this.acValidationTokenService = acValidationTokenService;
        this.mailService = mailService;
    }

    @Override
    public User findOne(Long id) throws UserNotFoundException {
        if (id == null)
            throw new IllegalArgumentException("\'id\' can not be null on UserServiceImpl.findOne(Long id)");
        User user = this.userRepo.findOne(id);
        if (user == null) throw new UserNotFoundException("Could not find user with id " + id);
        return user;
    }

    @Override
    public Page<User> findAllPaginated(int page) {
        return this.userRepo.findAll(PageAttr.getPageRequest(page));
    }

    @Override
    public User findByUsername(String username) throws UserNotFoundException {
        if (username == null)
            throw new IllegalArgumentException("\'username\' can not be null on UserServiceImpl.findByUsername(String username)");
        User user = this.userRepo.findByUsername(username);
        if (user == null) throw new UserNotFoundException("Could not find user with username " + username);
        return user;
    }

    @Override
    public User findByEmail(String email) throws UserNotFoundException {
        if (email == null)
            throw new IllegalArgumentException("\'email\' can not be null on UserServiceImpl.findByEmail(String email)");
        User user = this.userRepo.findByEmail(email);
        if (user == null) throw new UserNotFoundException("Could not find user with email " + email);
        return user;
    }

    @Override
    public User findByUsernameOrEmail(String usernameOrEmail) throws UserNotFoundException {
        User user = this.userRepo.findByUsername(usernameOrEmail);
        if (user == null)
            user = this.userRepo.findByEmail(usernameOrEmail);
        if (user == null)
            throw new UserNotFoundException("Could not find user with username or email " + usernameOrEmail);
        return user;
    }

    @Override
    public Page<User> findAll(int page, int size) {
        return this.userRepo.findAll(new PageRequest(page, size, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }

    @Override
    public User save(User user) throws UserAlreadyExistsException, UserInvalidException, NullPasswordException {
        if (!this.isValid(user)) throw new UserInvalidException("User unvalid");

        // check if user already exists
        if (user.getId() == null && this.exists(user))
            throw new UserAlreadyExistsException("User already exists with this email or username");
        if (user.getPassword()==null || user.getPassword().length()<6)
            throw new UserInvalidException("Password length must be at least 6 or more!");

        // set Address
        if (user.getAddress() == null) user.setAddress(new Address());
        // set Roles
        user.grantRole(this.roleService.findRole(Role.ERole.ROLE_USER));

        // Execute only when user is being registered
        if (user.getId() == null) {
            // Encrypt passwprd
            user.setPassword(PasswordUtil.encryptPassword(user.getPassword(), PasswordUtil.EncType.BCRYPT_ENCODER, null));
            if (user.getEmail().equals(this.adminEmail1) || user.getEmail().equals(this.adminEmail2))
                user.grantRole(this.roleService.findRole(Role.ERole.ROLE_ADMIN));
        }
        boolean newUser = user.getId()==null;
        user = this.userRepo.save(user);
        if (newUser) try {
            if (!user.isOnlyUser())
                this.requireAccountValidationByEmail(user.getEmail(), "/register/verify");
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public User saveDeveloper(User user) throws UserAlreadyExistsException, UserInvalidException, NullPasswordException {
        if (user == null) throw new UserInvalidException("User unvalid");
        user.grantRole(this.roleService.findRole(Role.ERole.ROLE_DEVELOPER));
        user = this.save(user);
        // create a client for that developer
        OAuth2Client client = new OAuth2Client();
        client = this.clientService.save(client);
        user.setClient(client);
        user = this.save(user);
        return user;
    }

    private boolean isValid(User user) {
        return user != null && user.getPassword() != null;
    }

    @Override
    public boolean exists(User user) {
        if (user == null) throw new IllegalArgumentException("user can't be null");
        return this.userRepo.findByEmail(user.getEmail()) != null || this.userRepo.findByUsername(user.getUsername()) != null;
    }

    @Override
    public User getAuthentication(String username, String password) throws UserNotFoundException, NullPasswordException {
        User user = this.findByUsernameOrEmail(username);
        if (PasswordUtil.matches(user.getPassword(), password))
            return user;
        return null;
    }

    @Override
    public User findDeveloperByUser(User user) {
        return this.findDeveloperByClient(user.getClient());
    }

    @Override
    public User findDeveloperByClient(OAuth2Client client) {
        Role role = this.roleService.findRole(Role.ERole.ROLE_DEVELOPER);
        if (role == null)
            role = this.roleService.save(new Role(Role.ERole.ROLE_DEVELOPER));
        return this.userRepo.findByClientAndRolesIn(client, role);
    }

    @Override
    public Page<User> getAllDevelopers(int page) {
        Role role = this.roleService.findRole(Role.ERole.ROLE_DEVELOPER);
        if (role == null)
            role = this.roleService.save(new Role(Role.ERole.ROLE_DEVELOPER));
        return this.userRepo.findByRolesIn(role,PageAttr.getPageRequest(page));
    }

    @Override
    public Page getUsersByDev(User devUser, int page, int size) {
        return this.userRepo.findByClient(devUser.getClient(), new PageRequest(page, size, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }

    @Override
    @Transactional
    public void updateUsersBalance(RShared rShared) {
        List<Long> userIds = this.userRepo.findIdsByClient(rShared.getDeveloper().getClient().getClientId());
        userIds.forEach(userId -> {
            User user = this.userRepo.findOne(userId);
            Transactions lastTrnx = this.trnxService.findLast(user);
            if (lastTrnx == null)
                lastTrnx = new Transactions();

            Transactions trnx = new Transactions();
            trnx.setBalance(lastTrnx.getBalance());
            trnx.setDebit(calculateRevenue(rShared, user));
            trnx.setFromDate(rShared.getFromDate());
            trnx.setToDate(rShared.getToDate());
            trnx.setUser(user);
            trnx.setrShared(rShared);
            trnx.setReference(rShared.getDeveloper().getName());
            trnx.setExplanation(rShared.getDeveloper().getName()+" shared you this amount according to the interaction point.");
            this.trnxService.save(trnx);
        });
    }

    @Override
    public Page<User> findByClientAndUsersNotInAndObeyThreshold(OAuth2Client client, List<Long> userIds, int page) {
        return this.userRepo.findByClientAndIdNotInAndCurrentBalanceGreaterThanEqual(client, userIds, this.thresholdLimit, new PageRequest(page, PageAttr.PAGE_SIZE, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }

    @Override
    public Page<User> findByClientAndUsersIn(OAuth2Client client, List<Long> paidUserIds, int page) {
        return this.userRepo.findByClientAndIdIn(client, paidUserIds, new PageRequest(page, PageAttr.PAGE_SIZE, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }

    @Override
    public Page<User> findByClientId(String clientId, int page) throws ClientNotFoundException {
        if (clientId==null) throw new ClientNotFoundException("Client id can not be null");
        return this.userRepo.findByClientClientId(clientId, PageAttr.getPageRequest(page));
    }

    @Override
    public Long getTotalNumberOfUsers(OAuth2Client client) {
        return this.userRepo.countAllByClient(client);
    }

    @Override
    public Long getTotalPayableAmount(OAuth2Client client) {
        return this.userRepo.sumOfAllUsersBalance(client.getId());
    }

    @Override
    public void requireAccountValidationByEmail(String email, String validationUrl) throws UserNotFoundException {
        if (email == null) throw new IllegalArgumentException("Email invalid!");
        User user = this.findByEmail(email);
        SessionIdentifierGenerator sessionIdentifierGenerator = new SessionIdentifierGenerator();
        AcValidationToken acValidationToken = new AcValidationToken();
        acValidationToken.setToken(sessionIdentifierGenerator.nextSessionId());
        acValidationToken.setTokenValid(true);
        acValidationToken.setUser(user);
        // save acvalidationtoken
        acValidationToken = this.acValidationTokenService.save(acValidationToken);
        if (validationUrl == null) {
            this.mailService.sendEmail(user.getEmail(), "ShareMyRevenue verification token", "Your verification token is: " + acValidationToken.getToken());
            return;
        }
        // build confirmation link
        String confirmationLink = baseUrlApi.trim() + validationUrl + "?token=" + acValidationToken.getToken() + "&enabled=true";
        // send link by email
        this.mailService.sendEmail(user.getEmail(), "Please verify you ShareMyRevenue account", "Please verify your email by clicking this link " + confirmationLink);
    }

    @Override
    @Transactional
    public User resetPassword(String token, String password) throws NullPasswordException, UserAlreadyExistsException, UserInvalidException {
        if (password.length() < 6)
            throw new IllegalArgumentException("Password length should be at least 6");
        AcValidationToken acValidationToken = this.acValidationTokenService.findByToken(token);
        User user = acValidationToken.getUser();
        user.setPassword(PasswordUtil.encryptPassword(password, PasswordUtil.EncType.BCRYPT_ENCODER, null));
        acValidationToken.setTokenValid(false);
        acValidationToken.setReason("Password Reset");
        user = this.save(user);
        acValidationToken.setUser(user);
        this.acValidationTokenService.save(acValidationToken);
        return user;
    }

    @Override
    public Page<User> findUsersIn(List<Long> userIds, int page) {
        return this.userRepo.findByIdIn(userIds, new PageRequest(page, PageAttr.PAGE_SIZE, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }


    private Long calculateRevenue(RShared rShared, User user) {
        Long totalInteractions = this.eventService.getUserClientEventsWeightSumInAPeriod(user, rShared.getFromDate(), rShared.getToDate());
        Long userInteractions = this.eventService.getUserEventsWeightSumInAPeriod(user, rShared.getFromDate(), rShared.getToDate());
        if (Validator.nullOrZero(totalInteractions) || Validator.nullOrZero(userInteractions)) return 0L;
        return (rShared.getSharedAmount() * userInteractions) / totalInteractions;
    }

}
