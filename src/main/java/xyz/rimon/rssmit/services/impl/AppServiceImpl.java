package xyz.rimon.rssmit.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import xyz.rimon.rssmit.commons.PageAttr;
import xyz.rimon.rssmit.domains.Application;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.repository.AppRepository;
import xyz.rimon.rssmit.services.AppService;
import xyz.rimon.rssmit.services.UserService;

@Service
public class AppServiceImpl implements AppService {

    private final AppRepository appRepo;
    private final UserService userService;

    @Autowired
    public AppServiceImpl(AppRepository appRepo, UserService userService) {
        this.appRepo = appRepo;
        this.userService = userService;
    }

    @Override
    public Application save(Application application) {
        if (application==null) throw new IllegalArgumentException("Application can not be null!");
        Application existing = this.findByPackageName(application.getPackageName());
        if (existing!=null) return existing;

        User devUser = this.userService.findDeveloperByClient(application.getClient());
        application.setDevId(devUser.getId());
        application.setDevName(devUser.getName());
        application.setDevEmail(devUser.getEmail());

        return this.appRepo.save(application);
    }

    @Override
    public Application findOne(Long id) {
        return this.appRepo.findOne(id);
    }

    @Override
    public Page<Application> findByClient(Long clientId,int page) {
        return this.appRepo.findByClientId(clientId,PageAttr.getPageRequest(page));
    }

    @Override
    public Page<Application> findAllPaginated(int page) {
        return this.appRepo.findAll(PageAttr.getPageRequest(page));
    }

    @Override
    public Application findByPackageName(String packageName) {
        return this.appRepo.findByPackageName(packageName);
    }
}
