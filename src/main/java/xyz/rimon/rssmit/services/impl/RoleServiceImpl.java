package xyz.rimon.rssmit.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.rimon.rssmit.domains.Role;
import xyz.rimon.rssmit.repository.RoleRepository;
import xyz.rimon.rssmit.services.RoleService;

@Service
public class RoleServiceImpl implements RoleService{
    private final RoleRepository roleRepo;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepo) {
        this.roleRepo = roleRepo;
    }

    @Override
    public Role findRole(Role.ERole eRole) {
        Role role = this.findByRole(eRole.toString());
        if (role==null) {
            role = new Role(eRole);
            role = this.save(role);
        }
        return role;
    }

    @Override
    public Role findByRole(String role) {
        if (role==null) throw new IllegalArgumentException("Role can not be null!");
        return this.roleRepo.findByRole(role);
    }

    @Override
    public Role save(Role role) {
        if (role==null) throw new IllegalArgumentException("Role can not be null!");
        return this.roleRepo.save(role);
    }
}
