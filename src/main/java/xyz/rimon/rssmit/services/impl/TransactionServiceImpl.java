package xyz.rimon.rssmit.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import xyz.rimon.rssmit.commons.PageAttr;
import xyz.rimon.rssmit.config.security.SecurityConfig;
import xyz.rimon.rssmit.domains.*;
import xyz.rimon.rssmit.exceptions.exists.UserAlreadyPaidException;
import xyz.rimon.rssmit.exceptions.invalid.InvalidException;
import xyz.rimon.rssmit.exceptions.invalid.PaymentRequstInvalidException;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.PaymentRequestNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.RSharedNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.repository.*;
import xyz.rimon.rssmit.services.PaymentService;
import xyz.rimon.rssmit.services.TransactionService;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService, PaymentService {

    private final TransactionRepository trnxRepo;
    private final PaymentRequestRepository paymentReqRepo;
    private final UserRepository userRepo;
    private final RSharedRepository rSharedRepo;
    private final ClientRepository clientRepo;

    @Value("${threshold-limit}")
    private Long thresholdLimit;

    @Autowired
    public TransactionServiceImpl(TransactionRepository trnxRepo,
                                  PaymentRequestRepository paymentReqRepo,
                                  UserRepository userRepo,
                                  RSharedRepository rSharedRepo, ClientRepository clientRepo) {
        this.trnxRepo = trnxRepo;
        this.paymentReqRepo = paymentReqRepo;
        this.userRepo = userRepo;
        this.rSharedRepo = rSharedRepo;
        this.clientRepo = clientRepo;
    }

    @Override
    @Transactional
    public Transactions save(Transactions trnx) {
        trnx = this.trnxRepo.save(trnx);
        User user = trnx.getUser();
        user.setCurrentBalance(trnx.getBalance());
        this.userRepo.save(user);
        return trnx;
    }

    @Override
    public Transactions findOne(Long id) {
        return this.trnxRepo.findOne(id);
    }

    @Override
    public Transactions findLast(User user) {
        return this.trnxRepo.findFirstByUserOrderByIdDesc(user);
    }

    @Override
    public List<Transactions> findPaymentTransactions(RShared rShared) throws RSharedNotFoundException {
        if (rShared == null) throw new RSharedNotFoundException();
        return this.trnxRepo.findByrSharedAndPaymentTrue(rShared);
    }

    @Override
    public List<Transactions> findSharingTransactions(RShared rShared) throws RSharedNotFoundException {
        if (rShared == null) throw new RSharedNotFoundException();
        return this.trnxRepo.findByrSharedAndPaymentFalse(rShared);
    }

    @Override
    public Page<Transactions> getTransactionsByClient(String clientId, Integer page) throws ClientNotFoundException {
        OAuth2Client client = this.clientRepo.findByClientId(clientId);
        if (client == null) throw new ClientNotFoundException("Could not find any client with client id");
        return this.trnxRepo.findByUserClient(client, new PageRequest(page, PageAttr.PAGE_SIZE, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }

    @Override
    public Long getTotalNumberOfTransaction(OAuth2Client client) {
        return this.trnxRepo.countByUserClient(client);
    }

    @Override
    public Transactions makePayment(Long userId, Long rShareId, Long amount, String trnxId) throws InvalidException {
        User user = this.userRepo.findOne(userId);
        RShared rShared;
        if (rShareId == null) // needed for payment rquests
            rShared = this.rSharedRepo.findFirstByDeveloperClientOrderByIdDesc(user.getClient());
        else // payment from sharing entity eligible users
            rShared = this.rSharedRepo.findOne(rShareId);
        if (user == null || rShared == null) throw new InvalidException("Invalid user or sharing entity");
        Transactions tranx = new Transactions();
        tranx.setBalance(user.getCurrentBalance());
        tranx.setCredit(amount);
        tranx.setTrnxId(trnxId);
        tranx.setReference("Paid by " + rShared.getDeveloper().getName());
        tranx.setExplanation("Developer " + rShared.getDeveloper().getName() + " acknowledged to pay " + amount + ". So it has been marked as complete.");
        tranx.setFromDate(rShared.getFromDate());
        tranx.setToDate(rShared.getToDate());
        tranx.setUser(user);
        tranx.setrShared(rShared);
        return this.save(tranx);
    }

    @Override
    public Transactions findUsersLastPayment(Long userId) {
        return this.trnxRepo.findFirstByUserIdAndPaymentTrueOrderByIdDesc(userId);
    }


    @Override
    public Page<Transactions> getPaymentsForUser(Long userId, int page) throws UserNotFoundException {
        User user = this.userRepo.findOne(userId);
        User loggedInUser = SecurityConfig.getCurrentUser();
        if (user==null || loggedInUser==null) throw new UserNotFoundException("Could not find user!");
        if (!loggedInUser.getClient().getClientId().equals(user.getClient().getClientId()) && !loggedInUser.isAdmin())
            throw new UserNotFoundException("Could not find user!");
        return this.trnxRepo.findByUser(user, new PageRequest(page, PageAttr.PAGE_SIZE, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }

    @Override
    public Page<PaymentRequest> getPaymentRequestsForUser(Long userId, int page) {
        return this.paymentReqRepo.findByUserId(userId, new PageRequest(page, PageAttr.PAGE_SIZE, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }

    @Override
    public PaymentRequest createPaymentRequest(PaymentRequest request, User user) throws UserAlreadyPaidException, PaymentRequstInvalidException {
        if (request == null) throw new IllegalArgumentException("Payment request can not be null!");
        // check if user is already paid this month
        Transactions lastTrnx = this.findUsersLastPayment(user.getId());
        if (lastTrnx != null && lastTrnx.isInCurrentMonth())
            throw new UserAlreadyPaidException("You are already paid this month!");
        if (user.getCurrentBalance() < thresholdLimit || user.getCurrentBalance() < request.getRequestAmount())
            throw new PaymentRequstInvalidException("Insufficient balance!");
        request.setUser(user);
        return this.paymentReqRepo.save(request);
    }

    @Override
    public Page<PaymentRequest> getPaymentRequestsForClient(String clientId, boolean isPaid, int page) throws UserNotFoundException {
        OAuth2Client client = this.clientRepo.findByClientId(clientId);
        if (client == null) throw new UserNotFoundException("Could not find client!");
        return isPaid ? this.paymentReqRepo.findByUserClientAndPaidTrue(client, new PageRequest(page, PageAttr.PAGE_SIZE, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID))
                : this.paymentReqRepo.findByUserClientAndPaidFalse(client, new PageRequest(page, PageAttr.PAGE_SIZE, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }

    @Override
    @Transactional
    public PaymentRequest resolveRequest(Long reqId, Long amount, String trnxId, User currentUser) throws InvalidException, PaymentRequestNotFoundException {
        PaymentRequest pr = this.paymentReqRepo.findOne(reqId);
        if (pr == null) throw new PaymentRequestNotFoundException("Payment request not found!");
        // check if loggedin user has permission to change payment for this user
        if (!currentUser.isAdmin() && !currentUser.hasSameClient(pr.getUser()))
            throw new InvalidException("You don\'t have permission to do this action!");
        Transactions trnx = this.makePayment(pr.getUser().getId(), null, amount, trnxId);
        pr.setTrnx(trnx);
        pr = this.paymentReqRepo.save(pr);
        return pr;
    }

    @Override
    public Long getTotalNumberOfPaymentRequests(OAuth2Client client) {
        return this.paymentReqRepo.countByUserClient(client);
    }

    @Override
    public boolean isUserAlreadyPaid(Long userId, Date fromDate, Date toDate) {
        Integer count = this.trnxRepo.countByUserIdAndCreatedBetweenAndPaymentTrue(userId, fromDate, toDate);
        return count != null && count != 0;
    }
}
