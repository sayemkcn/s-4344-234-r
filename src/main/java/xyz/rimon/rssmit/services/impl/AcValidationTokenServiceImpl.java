package xyz.rimon.rssmit.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.rimon.rssmit.domains.AcValidationToken;
import xyz.rimon.rssmit.repository.AcValidationTokenRepository;
import xyz.rimon.rssmit.services.AcValidationTokenService;

@Service
public class AcValidationTokenServiceImpl implements AcValidationTokenService {
    private final AcValidationTokenRepository tokenRepo;

    @Autowired
    public AcValidationTokenServiceImpl(AcValidationTokenRepository tokenRepo) {
        this.tokenRepo = tokenRepo;
    }

    @Override
    public AcValidationToken save(AcValidationToken acValidationToken) {
        return this.tokenRepo.save(acValidationToken);
    }

    @Override
    public AcValidationToken findOne(Long id) {
        return this.tokenRepo.findOne(id);
    }

    @Override
    public AcValidationToken findByToken(String token) {
        if (token == null) return null;
        return this.tokenRepo.findByToken(token);
    }

    @Override
    public void delete(Long id) {
        this.tokenRepo.delete(id);
    }

    @Override
    public boolean isTokenValid(String token) {
        if (token == null || token.isEmpty()) return false;
        AcValidationToken acValidationToken = this.findByToken(token);
        return acValidationToken != null && acValidationToken.isTokenValid();
    }
}
