package xyz.rimon.rssmit.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import xyz.rimon.rssmit.commons.PageAttr;
import xyz.rimon.rssmit.commons.utils.DateUtil;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.RShared;
import xyz.rimon.rssmit.domains.Transactions;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.domains.pojo.UserRev;
import xyz.rimon.rssmit.exceptions.exists.RSharedAlreadyExistsException;
import xyz.rimon.rssmit.exceptions.invalid.InvalidDateRangeException;
import xyz.rimon.rssmit.exceptions.notfound.NotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.RSharedNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.repository.PaymentRequestRepository;
import xyz.rimon.rssmit.repository.RSharedRepository;
import xyz.rimon.rssmit.services.*;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@PropertySource("classpath:payments.properties")
public class RSharedServiceImpl implements RSharedService {

    private final RSharedRepository rSharedRepo;
    private final UserService userService;
    private final EventService eventService;
    private final TransactionService trnxService;
    private final PaymentService paymentService;
    private final PaymentRequestRepository paymentRequestRepo;

    @Value("${threshold-limit}")
    private int thresholdLimit;

    @Autowired
    public RSharedServiceImpl(RSharedRepository rSharedRepo, UserService userService, EventService eventService, TransactionService trnxService, PaymentService paymentService, PaymentRequestRepository paymentRequestRepo) {
        this.rSharedRepo = rSharedRepo;
        this.userService = userService;
        this.eventService = eventService;
        this.trnxService = trnxService;
        this.paymentService = paymentService;
        this.paymentRequestRepo = paymentRequestRepo;
    }

    @Override
    @Transactional
    public RShared save(RShared rShared) throws RSharedAlreadyExistsException, InvalidDateRangeException {
        if (rShared.getId() == null && this.existsBetweenDates(rShared.getDeveloper(), rShared.getFromDate(), rShared.getToDate()))
            throw new RSharedAlreadyExistsException("Entity already exists within this period");
        if (rShared.getFromDate().after(rShared.getToDate()))
            throw new InvalidDateRangeException("fromDate can not be after toDate!");

        rShared = this.rSharedRepo.save(rShared);
        if (rShared.isActive())
            this.userService.updateUsersBalance(rShared);
        return rShared;
    }

    @Override
    public RShared findOne(Long id) throws NotFoundException {
        if (id == null) throw new IllegalArgumentException("\'id\' can not be null!");
        RShared rShared = this.rSharedRepo.findOne(id);
        if (rShared == null) throw new NotFoundException("Not ShareEntity found with id " + id);
        return rShared;
    }

    @Override
    public RShared findLastRShared(OAuth2Client client) {
        return this.rSharedRepo.findFirstByDeveloperClientOrderByIdDesc(client);
    }

    @Override
    public RShared findByPeriodAndDeveloper(DateUtil.Period period, User currentUser) {
        Map<DateUtil.DateRangeType, Calendar> dateMap = DateUtil.buildDateRange(period);
        Date fromDate = dateMap.get(DateUtil.DateRangeType.DATE_FROM).getTime();
        Date toDate = dateMap.get(DateUtil.DateRangeType.DATE_TO).getTime();
        return this.rSharedRepo.findByDeveloperAndFromDateOrToDate(currentUser, fromDate, toDate);
    }

    @Override
    public Page<RShared> findAllByDeveloper(User currentUser, int page, int size) {
        return this.rSharedRepo.findAllByDeveloper(currentUser, new PageRequest(Math.abs(page), size, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
    }

    @Override
    public RShared findByUserInAPeriod(User user, Date fromDate, Date toDate) {
        User devUser = this.userService.findDeveloperByUser(user);
        return this.rSharedRepo.findByDeveloperAndFromDateBetween(devUser, fromDate, toDate);
    }

    @Override
    public UserRev calculateRevenue(User user, String month, int year) throws RSharedNotFoundException, UserNotFoundException {
        Map<DateUtil.DateRangeType, Calendar> dateMap = DateUtil.buildDateRangeWithMonth(month, year);
        UserRev userRev = new UserRev();

        // find this current month user interation points
        Calendar fromCal = dateMap.get(DateUtil.DateRangeType.DATE_FROM);
        Calendar toCal = dateMap.get(DateUtil.DateRangeType.DATE_TO);
        Date fromDate = fromCal.getTime();
        Date toDate = toCal.getTime();
        userRev.setFrom(fromDate);
        userRev.setTo(toDate);

        // check if user already has payment request this month
        int count = this.paymentRequestRepo.countByUserIdAndCreatedBetween(user.getId(), fromDate, toDate);
        if (count > 0 ||
                user.getCurrentBalance() < this.thresholdLimit ||
                this.paymentService.isUserAlreadyPaid(user.getId(), fromDate, toDate))
            userRev.setCanRequestForPayment(false);
        else userRev.setCanRequestForPayment(true);
        userRev.setThresholdLimit(this.thresholdLimit);

//        Long totalInteractionPointsCurrentMonth = this.eventService.getUserClientEventsWeightSumInAPeriod(user, fromDate, toDate);
        Long userInteractionPointsCurrentMonth = this.eventService.getUserEventsWeightSumInAPeriod(user, fromDate, toDate);
        if (userInteractionPointsCurrentMonth == null) userInteractionPointsCurrentMonth = 0L;
        userRev.setCurrentMonthInteractionPoints(userInteractionPointsCurrentMonth);
        user = this.userService.findOne(user.getId());
        userRev.setCurrentBalance(user.getCurrentBalance());

        // set last payment amount
        Transactions lastTrnx = this.paymentService.findUsersLastPayment(user.getId());
        if (lastTrnx != null) userRev.setLastPaymentAmount(lastTrnx.getCredit());

        // find last month user earning
        fromCal.add(Calendar.MONTH, -1);
        toCal.add(Calendar.MONTH, -1);
        fromDate = fromCal.getTime();
        toDate = toCal.getTime();

        Long totalInteractionPointsPreviousMonth = this.eventService.getUserClientEventsWeightSumInAPeriod(user, fromDate, toDate);
        Long userInterationsPointsPreviousMonth = this.eventService.getUserEventsWeightSumInAPeriod(user, fromDate, toDate);
        if (userInterationsPointsPreviousMonth == null) userInterationsPointsPreviousMonth = 0L;
        RShared rSharedPreviousMonth = this.findByUserInAPeriod(user, fromDate, toDate);

        if (rSharedPreviousMonth != null && totalInteractionPointsPreviousMonth != null) {
            double perWeightCost = (double) rSharedPreviousMonth.getSharedAmount() / (double) totalInteractionPointsPreviousMonth;
            Long individualIncome = (long) ((double)userInterationsPointsPreviousMonth * perWeightCost);
            userRev.setPreviousMonthIncome(individualIncome);
        }

        return userRev;
    }

    @Override
    public Page<User> findEligibleUsersForPayment(Long rSharedId, User currentUser, int page) throws RSharedNotFoundException {
        List<Long> paidUserIds = getPaidUserIds(rSharedId, currentUser);
        return this.userService.findByClientAndUsersNotInAndObeyThreshold(currentUser.getClient(), paidUserIds, page);
    }

    @Override
    public Page<User> findAlreadyPaidUsersForPayment(Long rSharedId, User currentUser, int page) throws RSharedNotFoundException {
        List<Long> paidUserIds = getPaidUserIds(rSharedId, currentUser);
        return this.userService.findByClientAndUsersIn(currentUser.getClient(), paidUserIds, page);
    }

    @Override
    public Page<RShared> findAllRShared(boolean isActive, int page) {
        Page<RShared> rSharedPage;
        if (isActive)
            rSharedPage = this.rSharedRepo.findByActiveTrue(new PageRequest(page, PageAttr.PAGE_SIZE, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
        else
            rSharedPage = this.rSharedRepo.findByActiveFalse(new PageRequest(page, PageAttr.PAGE_SIZE, Sort.Direction.DESC, PageAttr.SORT_BY_FIELD_ID));
        return rSharedPage;
    }

    @Override
    public Long getTotalSharedAmount(OAuth2Client client) {
        Long total = this.rSharedRepo.sumOfAllSharedAmounts(client.getId());
        if (total==null) return 0L;
        return total;
    }

    private List<Long> getPaidUserIds(Long rSharedId, User currentUser) throws RSharedNotFoundException {
        RShared rShared = this.rSharedRepo.findByIdAndDeveloper(rSharedId, currentUser);
        if (rShared == null) throw new RSharedNotFoundException("Could not find shared entity!");
        List<Transactions> paymentTrnxs = this.trnxService.findPaymentTransactions(rShared);
        List<Long> paidUserIds = paymentTrnxs.stream()
//                .filter(trnx -> trnx.getUser().getCurrentBalance() > 1000)
                .map(trnx -> trnx.getUser().getId())
                .collect(Collectors.toList());
        if (paidUserIds.isEmpty())
            paidUserIds.add(7447836437438947383L); // JPA QUERY 'NOT IN' DOESN'T WORK WITH EMPTY LIST, SO HAD TO USE IF FOR SOME REASON
        return paidUserIds;
    }

    private boolean existsBetweenDates(User devUser, Date fromDate, Date toDate) {
        boolean exits1 = this.rSharedRepo.findByDeveloperAndFromDateBetween(devUser, fromDate, toDate) != null;
        boolean exists2 = this.rSharedRepo.findByDeveloperAndToDateBetween(devUser, fromDate, toDate) != null;
        return exits1 || exists2;
    }
}
