package xyz.rimon.rssmit.services;

import xyz.rimon.rssmit.domains.Statistics;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;

public interface StatisticsService {
    Statistics getStatistics(String ClientId, String period) throws ClientNotFoundException;
}
