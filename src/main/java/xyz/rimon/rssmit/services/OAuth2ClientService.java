package xyz.rimon.rssmit.services;

import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;

public interface OAuth2ClientService {
    OAuth2Client save(OAuth2Client client);
    OAuth2Client findOne(Long id) throws ClientNotFoundException;
    OAuth2Client findByClientId(String clientId, String clientSecret) throws ClientNotFoundException;
    OAuth2Client findByUser(User user);
}
