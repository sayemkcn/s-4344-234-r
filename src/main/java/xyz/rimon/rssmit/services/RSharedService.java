package xyz.rimon.rssmit.services;

import org.springframework.data.domain.Page;
import xyz.rimon.rssmit.commons.utils.DateUtil;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.RShared;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.domains.pojo.UserRev;
import xyz.rimon.rssmit.exceptions.exists.RSharedAlreadyExistsException;
import xyz.rimon.rssmit.exceptions.invalid.InvalidDateRangeException;
import xyz.rimon.rssmit.exceptions.notfound.NotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.RSharedNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;

import java.util.Date;


public interface RSharedService {
    RShared save(RShared rShared) throws RSharedAlreadyExistsException, InvalidDateRangeException;
    RShared findOne(Long id) throws NotFoundException;
    RShared findLastRShared(OAuth2Client client);
    RShared findByPeriodAndDeveloper(DateUtil.Period period, User currentUser);

    Page<RShared> findAllByDeveloper(User currentUser, int page, int pageSize);

    RShared findByUserInAPeriod(User user, Date fromDate, Date toDate);
    UserRev calculateRevenue(User user, String month,int year) throws RSharedNotFoundException, UserNotFoundException;

    Page<User> findEligibleUsersForPayment(Long rSharedId, User currentUser, int page) throws RSharedNotFoundException;

    Page<User> findAlreadyPaidUsersForPayment(Long rSharedId, User currentUser, int page) throws RSharedNotFoundException;

    Page<RShared> findAllRShared(boolean isActive, int page);

    Long getTotalSharedAmount(OAuth2Client client);
}
