package xyz.rimon.rssmit.services;

import xyz.rimon.rssmit.domains.Role;

public interface RoleService {
    Role findRole(Role.ERole role);
    Role findByRole(String role);
    Role save(Role role);
}
