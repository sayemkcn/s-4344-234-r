package xyz.rimon.rssmit.services;

import org.springframework.data.domain.Page;
import xyz.rimon.rssmit.domains.Application;

public interface AppService {
    Application save(Application application);
    Application findOne(Long id);
    Page<Application> findByClient(Long clientId,int page);
    Page<Application> findAllPaginated(int page);
    Application findByPackageName(String packageName);
}
