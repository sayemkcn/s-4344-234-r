package xyz.rimon.rssmit.services;

import org.springframework.data.domain.Page;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.RShared;
import xyz.rimon.rssmit.domains.Transactions;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.RSharedNotFoundException;

import java.util.Date;
import java.util.List;

public interface TransactionService {
    Transactions save(Transactions trnx);
    Transactions findOne(Long id);
    Transactions findLast(User user);

    List<Transactions> findPaymentTransactions(RShared rShared) throws RSharedNotFoundException;
    List<Transactions> findSharingTransactions(RShared rShared) throws RSharedNotFoundException;

    Page<Transactions> getTransactionsByClient(String clientId, Integer page) throws ClientNotFoundException;

    Long getTotalNumberOfTransaction(OAuth2Client client);
}
