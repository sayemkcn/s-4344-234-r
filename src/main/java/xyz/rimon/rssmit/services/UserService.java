package xyz.rimon.rssmit.services;

import org.springframework.data.domain.Page;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.RShared;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.exceptions.exists.UserAlreadyExistsException;
import xyz.rimon.rssmit.exceptions.invalid.UserInvalidException;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.exceptions.nullpointers.NullPasswordException;

import java.util.List;

public interface UserService {
    User findOne(Long id) throws UserNotFoundException;

    Page<User> findAllPaginated(int page);

    User findByUsername(String username) throws UserNotFoundException;

    User findByEmail(String email) throws UserNotFoundException;

    User findByUsernameOrEmail(String usernameOrEmail) throws UserNotFoundException;

    Page<User> findAll(int page, int size);

    User save(User user) throws UserAlreadyExistsException, UserInvalidException, NullPasswordException;

    User saveDeveloper(User user) throws UserAlreadyExistsException, UserInvalidException, NullPasswordException;

    boolean exists(User user);

    User getAuthentication(String username, String password) throws UserNotFoundException, NullPasswordException;

    User findDeveloperByUser(User user);
    User findDeveloperByClient(OAuth2Client client);

    Page<User> getAllDevelopers(int page);

    Page getUsersByDev(User devUser,int page,int size);

    void updateUsersBalance(RShared rShared);

//    Page<User> findUsersNotInAndObeyThreshold(List<Long> userIds, int page);

    Page<User> findUsersIn(List<Long> userIds, int page);

    Page<User> findByClientAndUsersNotInAndObeyThreshold(OAuth2Client client, List<Long> paidUserIds, int page);
    Page<User> findByClientAndUsersIn(OAuth2Client client, List<Long> paidUserIds, int page);

    Page<User> findByClientId(String clientId, int page) throws ClientNotFoundException;

    Long getTotalNumberOfUsers(OAuth2Client client);

    Long getTotalPayableAmount(OAuth2Client client);

    void requireAccountValidationByEmail(String email, String validationUrl) throws UserNotFoundException;

    User resetPassword(String token, String password) throws NullPasswordException, UserAlreadyExistsException, UserInvalidException;
}
