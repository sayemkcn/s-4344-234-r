package xyz.rimon.rssmit.services;

import xyz.rimon.rssmit.domains.AcValidationToken;

public interface AcValidationTokenService {
    AcValidationToken save(AcValidationToken acValidationToken);
    AcValidationToken findOne(Long id);
    AcValidationToken findByToken(String token);
    void delete(Long id);
    boolean isTokenValid(String token);
}
