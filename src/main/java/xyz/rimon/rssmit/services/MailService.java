package xyz.rimon.rssmit.services;

public interface MailService {
    void sendEmail(String email, String subject, String message);
}
