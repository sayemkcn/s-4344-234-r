package xyz.rimon.rssmit.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.Role;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.domains.annotations.CurrentUser;
import xyz.rimon.rssmit.exceptions.exists.UserAlreadyExistsException;
import xyz.rimon.rssmit.exceptions.invalid.UserInvalidException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.exceptions.nullpointers.NullPasswordException;
import xyz.rimon.rssmit.services.OAuth2ClientService;
import xyz.rimon.rssmit.services.UserService;

import java.util.Map;

@RestController
@RequestMapping("/dev")
public class DevController {

    private final UserService userService;
    private final OAuth2ClientService clientService;

    @Autowired
    public DevController(UserService userService, OAuth2ClientService clientService) {
        this.userService = userService;
        this.clientService = clientService;
    }

    @PostMapping("/register")
    private ResponseEntity register(@ModelAttribute User user, BindingResult bindingResult) throws UserAlreadyExistsException, UserInvalidException, NullPasswordException {
        if (bindingResult.hasErrors())
            return ResponseEntity.badRequest().build();
        if (this.userService.exists(user)) return ResponseEntity
                .status(HttpStatus.IM_USED).build();

        user = this.userService.saveDeveloper(user);
        return ResponseEntity.ok(user);
    }

    @GetMapping("/client/credentials")
    private ResponseEntity getClientCredentials(@RequestParam("username") String username,
                                                @RequestParam("password") String password) throws UserNotFoundException, NullPasswordException {
        User user = this.userService.getAuthentication(username, password);
        if (user == null || !user.hasRole(Role.ERole.ROLE_DEVELOPER.toString())) return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        OAuth2Client client = this.clientService.findByUser(user);
        if (client == null) return ResponseEntity.noContent().build();
        return ResponseEntity.ok(client);
    }
//    @PostMapping("/client/create")
//    private ResponseEntity clientRegister(@ModelAttribute OAuth2Client client, BindingResult bindingResult) {
//        if (bindingResult.hasErrors())
//            return ResponseEntity.badRequest().build();
//        client = clientService.save(client);
//        return ResponseEntity.ok(client);
//    }

}
