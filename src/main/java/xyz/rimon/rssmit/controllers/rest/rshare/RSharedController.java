package xyz.rimon.rssmit.controllers.rest.rshare;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.domains.annotations.CurrentUser;
import xyz.rimon.rssmit.domains.pojo.UserRev;
import xyz.rimon.rssmit.exceptions.notfound.RSharedNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.services.RSharedService;

@Controller
@RequestMapping("/api/v1/rev")
public class RSharedController {
    private final RSharedService rSharedService;

    @Autowired
    public RSharedController(RSharedService rSharedService) {
        this.rSharedService = rSharedService;
    }

    // calculate
    @GetMapping("/self")
    private ResponseEntity getIncome(@CurrentUser User user,
                                     @RequestParam(value = "month") String month,
                                     @RequestParam(value = "year") Integer year) throws RSharedNotFoundException, UserNotFoundException {
        UserRev rev = this.rSharedService.calculateRevenue(user, month, year);
        return ResponseEntity.ok(rev);
    }
}
