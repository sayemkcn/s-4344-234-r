package xyz.rimon.rssmit.controllers.rest.payments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xyz.rimon.rssmit.domains.Transactions;
import xyz.rimon.rssmit.exceptions.invalid.InvalidException;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.services.PaymentService;
import xyz.rimon.rssmit.services.TransactionService;

@RestController
@RequestMapping("/api/v1/transactions")
public class TransactionController {

    private final PaymentService paymentService;
    private final TransactionService trnxService;

    @Autowired
    public TransactionController(PaymentService paymentService, TransactionService trnxService) {
        this.paymentService = paymentService;
        this.trnxService = trnxService;
    }

    @GetMapping("/byClient/{clientId}")
    private ResponseEntity getTransactionsByClient(@PathVariable("clientId") String clientId,
                                                   @RequestParam(value = "page", defaultValue = "0") Integer page) throws ClientNotFoundException {
        Page<Transactions> trnxPage = this.trnxService.getTransactionsByClient(clientId, page);
        return ResponseEntity.ok(trnxPage);
    }

    @GetMapping("/{userId}")
    private ResponseEntity getUserTransactions(@PathVariable("userId") Long userId,
                                               @RequestParam(value = "page", defaultValue = "0") Integer page) throws UserNotFoundException {
        Page<Transactions> trnxPage = this.paymentService.getPaymentsForUser(userId, page);
        return ResponseEntity.ok(trnxPage);
    }

    @PostMapping("/create")
    private ResponseEntity createPayment(@RequestParam("userId") Long userId,
                                         @RequestParam("rShareId") Long rShareId,
                                         @RequestParam("amount") Long amount,
                                         @RequestParam("trnxId") String trnxId) throws InvalidException {
        Transactions tranx = this.paymentService.makePayment(userId, rShareId, amount,trnxId);
        return ResponseEntity.ok(tranx);
    }

}
