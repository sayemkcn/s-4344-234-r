package xyz.rimon.rssmit.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.rimon.rssmit.commons.SessionAttr;
import xyz.rimon.rssmit.domains.AcValidationToken;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.exceptions.exists.UserAlreadyExistsException;
import xyz.rimon.rssmit.exceptions.invalid.UserInvalidException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.exceptions.nullpointers.NullPasswordException;
import xyz.rimon.rssmit.services.AcValidationTokenService;
import xyz.rimon.rssmit.services.UserService;

import javax.transaction.Transactional;

@Controller
public class UserController {

    private final UserService userService;
    private final AcValidationTokenService acValidationTokenService;

    @Value("${baseUrl}")
    String baseUrl;

    @Autowired
    public UserController(UserService userService, AcValidationTokenService acValidationTokenService) {
        this.userService = userService;
        this.acValidationTokenService = acValidationTokenService;
    }

    @GetMapping("/")
    private String sayHello() {
        return "Hello World!";
    }

    @PostMapping("/register")
    private ResponseEntity register(@ModelAttribute User user, BindingResult bindingResult) throws UserAlreadyExistsException, UserInvalidException, NullPasswordException {
        if (bindingResult.hasErrors())
            return ResponseEntity.badRequest().build();
        if (this.userService.exists(user)) return ResponseEntity
                .status(HttpStatus.IM_USED).build();

        user = this.userService.save(user);
        return ResponseEntity.ok(user);
    }


    // Password reset
    @PostMapping("/resetPassword/verifyEmail")
    private ResponseEntity verifyEmail(@RequestParam("email") String email) throws UserNotFoundException {
        this.userService.requireAccountValidationByEmail(email, null);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping("/checkTokenValidity")
    private ResponseEntity checkTokenValidity(@RequestParam(value = "token") String token) {
        if (!this.acValidationTokenService.isTokenValid(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        return ResponseEntity.ok(token);
    }

    @PostMapping("/resetPassword")
    private ResponseEntity resetPassword(@RequestParam("token") String token,
                                         @RequestParam("password") String password) throws Exception, NullPasswordException, UserAlreadyExistsException, UserInvalidException {
        if (!this.acValidationTokenService.isTokenValid(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        User user = this.userService.resetPassword(token, password);
        return ResponseEntity.ok(user);
    }

    // Verify email when registration
    @GetMapping("/register/verify")
    @Transactional
    String verifyRegistration(@RequestParam("token") String token) throws Exception, NullPasswordException, UserAlreadyExistsException, UserInvalidException {
        if (!this.acValidationTokenService.isTokenValid(token))
            return "redirect:" + this.baseUrl + "/login?verify=false";
        AcValidationToken acValidationToken = this.acValidationTokenService.findByToken(token);
        if (acValidationToken == null) return "redirect:" + this.baseUrl + "/login?verify=false";
        User user = acValidationToken.getUser();
        user.setEnabled(true);
        this.userService.save(user);

        acValidationToken.setTokenValid(false);
        acValidationToken.setReason("Registration/Email Confirmation");
        this.acValidationTokenService.save(acValidationToken);
        return "redirect:" + this.baseUrl + "/login?verify=true";
    }

}
