package xyz.rimon.rssmit.controllers.rest.apps.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.rimon.rssmit.services.AppService;

@RestController
@RequestMapping("/api/v1/admin/apps")
public class AppsAdminController {

    private final AppService appService;

    @Autowired
    public AppsAdminController(AppService appService) {
        this.appService = appService;
    }

    @GetMapping("")
    private ResponseEntity getAllApps(@RequestParam(value = "page", defaultValue = "0") Integer page) {
        return ResponseEntity.ok(this.appService.findAllPaginated(page));
    }
}
