package xyz.rimon.rssmit.controllers.rest.payments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xyz.rimon.rssmit.domains.PaymentRequest;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.domains.annotations.CurrentUser;
import xyz.rimon.rssmit.exceptions.exists.UserAlreadyPaidException;
import xyz.rimon.rssmit.exceptions.invalid.InvalidException;
import xyz.rimon.rssmit.exceptions.invalid.PaymentRequstInvalidException;
import xyz.rimon.rssmit.exceptions.notfound.PaymentRequestNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.services.PaymentService;

@RestController
@RequestMapping("/api/v1/payments")
public class PaymentController {

    private final PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @GetMapping("/requests/{clientId}")
    private ResponseEntity getPaymentRequests(@PathVariable("clientId") String clientId,
                                              @RequestParam(value = "paid", defaultValue = "false") Boolean isPaid,
                                              @RequestParam(value = "page", defaultValue = "0") Integer page) throws UserNotFoundException {
        return ResponseEntity.ok(this.paymentService.getPaymentRequestsForClient(clientId, isPaid, page));
    }

    @PostMapping("/requests")
    private ResponseEntity createRequest(@ModelAttribute PaymentRequest request,
                                         @CurrentUser User currentUser) throws UserAlreadyPaidException, PaymentRequstInvalidException {
        return ResponseEntity.ok(this.paymentService.createPaymentRequest(request, currentUser));
    }

    @PostMapping("/requests/{reqId}/resolve")
    private ResponseEntity resolvePaymentRequest(@PathVariable("reqId") Long reqId,
                                                 @RequestParam("amount") Long amount,
                                                 @RequestParam("trnxId") String trnxId,
                                                 @CurrentUser User currentUser) throws InvalidException, PaymentRequestNotFoundException {
        PaymentRequest request = this.paymentService.resolveRequest(reqId, amount, trnxId, currentUser);
        return ResponseEntity.ok(request);
    }

}
