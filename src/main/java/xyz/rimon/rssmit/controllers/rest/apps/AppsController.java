package xyz.rimon.rssmit.controllers.rest.apps;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.domains.annotations.CurrentUser;
import xyz.rimon.rssmit.services.AppService;

@RestController
@RequestMapping("/api/v1/apps")
public class AppsController {
    private final AppService appService;

    @Autowired
    public AppsController(AppService appService) {
        this.appService = appService;
    }

    @GetMapping("")
    private ResponseEntity getMyApps(@CurrentUser User user,
                                     @RequestParam(value = "page",defaultValue = "0") Integer page){
        return ResponseEntity.ok(this.appService.findByClient(user.getClient().getId(),page));
    }
}
