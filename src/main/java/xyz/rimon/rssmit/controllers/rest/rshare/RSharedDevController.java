package xyz.rimon.rssmit.controllers.rest.rshare;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import xyz.rimon.rssmit.commons.PageAttr;
import xyz.rimon.rssmit.commons.utils.DateUtil;
import xyz.rimon.rssmit.domains.RShared;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.domains.annotations.CurrentUser;
import xyz.rimon.rssmit.exceptions.exists.RSharedAlreadyExistsException;
import xyz.rimon.rssmit.exceptions.invalid.InvalidDateRangeException;
import xyz.rimon.rssmit.exceptions.notfound.RSharedNotFoundException;
import xyz.rimon.rssmit.services.RSharedService;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/rshared")
public class RSharedDevController {

    private final RSharedService rSharedService;

    @Autowired
    public RSharedDevController(RSharedService rSharedService) {
        this.rSharedService = rSharedService;
    }

    @PostMapping("")
    private ResponseEntity create(@ModelAttribute RShared rShared, BindingResult bindingResult,
                                  @RequestParam("month") String month,
                                  @RequestParam(value = "year", defaultValue = "2018") Integer year,
                                  @CurrentUser User currentUser) throws RSharedAlreadyExistsException, InvalidDateRangeException {
        if (bindingResult.hasErrors() || rShared == null)
            return ResponseEntity.badRequest().build();
        Map<DateUtil.DateRangeType, Calendar> dateMap = DateUtil.buildDateRangeWithMonth(month, year);
        Date fromDate = dateMap.get(DateUtil.DateRangeType.DATE_FROM).getTime();
        Date toDate = dateMap.get(DateUtil.DateRangeType.DATE_TO).getTime();
        rShared.setFromDate(fromDate);
        rShared.setToDate(toDate);
        rShared.setDeveloper(currentUser);
        rShared = this.rSharedService.save(rShared);
        return ResponseEntity.ok(rShared);
    }

    @GetMapping("/{rSharedId}/eligibleUsers")
    private ResponseEntity getEligibleUsers(@PathVariable("rSharedId") Long rSharedId,
                                            @CurrentUser User currentUser,
                                            @RequestParam(value = "page", defaultValue = "0") Integer page) throws RSharedNotFoundException {
        Page<User> eligibleUsers = this.rSharedService.findEligibleUsersForPayment(rSharedId, currentUser, page);
        return ResponseEntity.ok(eligibleUsers);
    }

    @GetMapping("/{rSharedId}/paidUsers")
    private ResponseEntity getPaidUsers(@PathVariable("rSharedId") Long rSharedId,
                                        @CurrentUser User currentUser,
                                        @RequestParam(value = "page", defaultValue = "0") Integer page) throws RSharedNotFoundException {
        Page<User> eligibleUsers = this.rSharedService.findAlreadyPaidUsersForPayment(rSharedId, currentUser, page);
        return ResponseEntity.ok(eligibleUsers);
    }

    @GetMapping("/lastMonth")
    private ResponseEntity getLastMonthsSharing(@CurrentUser User currentUser) {
        RShared rSharedLastMonth = this.rSharedService.findByPeriodAndDeveloper(DateUtil.Period.LAST_MONTH, currentUser);
        return ResponseEntity.ok(rSharedLastMonth);
    }

    @GetMapping("")
    private ResponseEntity getRShared(@CurrentUser User currentUser,
                                      @RequestParam(value = "page", required = false, defaultValue = "0") Integer page) {
        Page<RShared> rSharedPage = this.rSharedService.findAllByDeveloper(currentUser, page, PageAttr.PAGE_SIZE);
        return ResponseEntity.ok(rSharedPage);
    }


}
