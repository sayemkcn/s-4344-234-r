package xyz.rimon.rssmit.controllers.rest.users.endusers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import xyz.rimon.rssmit.domains.Application;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.domains.annotations.CurrentUser;
import xyz.rimon.rssmit.domains.pojo.UserRev;
import xyz.rimon.rssmit.exceptions.exists.UserAlreadyExistsException;
import xyz.rimon.rssmit.exceptions.invalid.UserInvalidException;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.RSharedNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.exceptions.nullpointers.NullPasswordException;
import xyz.rimon.rssmit.services.AppService;
import xyz.rimon.rssmit.services.OAuth2ClientService;
import xyz.rimon.rssmit.services.RSharedService;
import xyz.rimon.rssmit.services.UserService;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/api/v1/users")
public class DevUsersController {
    private final UserService userService;
    private final OAuth2ClientService clientService;
    private final RSharedService rSharedService;
    private final AppService appService;

    @Autowired
    public DevUsersController(UserService userService,
                              OAuth2ClientService clientService,
                              RSharedService rSharedService,
                              AppService appService) {
        this.userService = userService;
        this.clientService = clientService;
        this.rSharedService = rSharedService;
        this.appService = appService;
    }

    @GetMapping("")
    private ResponseEntity getUsers(@CurrentUser User currentUser,
                                    @RequestParam(value = "page", defaultValue = "0") Integer page,
                                    @RequestParam(value = "size", defaultValue = "10") Integer size) {
        Page usersPage = this.userService.getUsersByDev(currentUser, page, size);
        return ResponseEntity.ok(usersPage);
    }

    @GetMapping("/{userId}/rev")
    private ResponseEntity getRevForAUser(@PathVariable("userId") Long userId,
                                          @RequestParam(value = "month") String month,
                                          @RequestParam(value = "year") Integer year) throws UserNotFoundException, RSharedNotFoundException {
        User user = this.userService.findOne(userId);
        UserRev rev = this.rSharedService.calculateRevenue(user, month, year);
        return ResponseEntity.ok(rev);
    }

    @PostMapping("/create")
    @Transactional
    ResponseEntity createUser(@ModelAttribute User user, BindingResult bindingResult,
                              @RequestParam("clientId") String clientId,
                              @RequestParam("clientSecret") String clientSecret,
                              @RequestParam(value = "appName", required = false) String appName,
                              @RequestParam(value = "appPackageName", required = false) String appPackageName) throws UserAlreadyExistsException, UserInvalidException, ClientNotFoundException, NullPasswordException {
        if (bindingResult.hasErrors() || user == null) return ResponseEntity.badRequest().build();
        OAuth2Client client = this.clientService.findByClientId(clientId, clientSecret);
        user.setClient(client);
        // add a namespace on unique fields for users
        user.setUsername(client.getClientIdSnapshot() + "__" + user.getUsername());
        user.setEmail(client.getClientIdSnapshot() + "__" + user.getEmail());
        user.setEnabled(true);

        user = this.userService.save(user);

        if (appPackageName != null && !appPackageName.isEmpty()) {
            Application app = new Application(appName, appPackageName, user.getClient());
            this.appService.save(app);
        }

        return ResponseEntity.ok(user);
    }

}
