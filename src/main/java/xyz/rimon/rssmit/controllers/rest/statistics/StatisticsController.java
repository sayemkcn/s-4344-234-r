package xyz.rimon.rssmit.controllers.rest.statistics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.rimon.rssmit.domains.Statistics;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;
import xyz.rimon.rssmit.services.StatisticsService;

@Controller
@RequestMapping("/api/v1/stats")
public class StatisticsController {
    private final StatisticsService statisticsService;

    @Autowired
    public StatisticsController(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @GetMapping("/{clientId}")
    private ResponseEntity getStatsForAClient(@PathVariable("clientId") String clientId,
                                              @RequestParam(value = "period", defaultValue = "this_month") String period) throws ClientNotFoundException {
        Statistics statistics = this.statisticsService.getStatistics(clientId, period);
        return ResponseEntity.ok(statistics);
    }

}
