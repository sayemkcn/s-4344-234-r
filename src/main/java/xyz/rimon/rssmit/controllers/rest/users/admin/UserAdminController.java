package xyz.rimon.rssmit.controllers.rest.users.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;
import xyz.rimon.rssmit.services.UserService;

@RestController
@RequestMapping("/api/v1/admin/users")
public class UserAdminController {

    private final UserService userService;

    @Autowired
    public UserAdminController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("")
    private ResponseEntity getAllUsersPaginated(@RequestParam(value = "clientId", required = false) String clientId,
                                                @RequestParam(value = "page", defaultValue = "0") Integer page) throws ClientNotFoundException {
        Page<User> userPage;
        if (clientId != null)
            userPage = this.userService.findByClientId(clientId, page);
        else
            userPage = this.userService.findAllPaginated(page);
        return ResponseEntity.ok(userPage);
    }

    @GetMapping("/devs")
    private ResponseEntity getAllDevs(@RequestParam(value = "page",defaultValue = "0") Integer page){
        Page<User> developersPage = this.userService.getAllDevelopers(page);
        return ResponseEntity.ok(developersPage);
    }
}
