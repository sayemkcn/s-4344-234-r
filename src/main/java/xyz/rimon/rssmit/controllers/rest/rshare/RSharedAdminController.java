package xyz.rimon.rssmit.controllers.rest.rshare;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xyz.rimon.rssmit.domains.RShared;
import xyz.rimon.rssmit.domains.Role;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.domains.annotations.CurrentUser;
import xyz.rimon.rssmit.exceptions.exists.RSharedAlreadyExistsException;
import xyz.rimon.rssmit.exceptions.invalid.InvalidDateRangeException;
import xyz.rimon.rssmit.exceptions.notfound.NotFoundException;
import xyz.rimon.rssmit.services.RSharedService;

@RestController
@RequestMapping("/api/v1/admin/rshared")
public class RSharedAdminController {
    private final RSharedService rSharedService;

    @Autowired
    public RSharedAdminController(RSharedService rSharedService) {
        this.rSharedService = rSharedService;
    }

    @GetMapping("")
    private ResponseEntity getRShared(@CurrentUser User currentUser,
                                      @RequestParam(value = "active", defaultValue = "false") Boolean isActive,
                                      @RequestParam(value = "page", required = false, defaultValue = "0") Integer page) {
        if (!currentUser.hasRole(Role.ERole.ROLE_ADMIN.toString()))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        Page<RShared> rSharedPage = this.rSharedService.findAllRShared(isActive, page);
        return ResponseEntity.ok(rSharedPage);
    }

    @PostMapping("/{rSharedId}/activate")
    private ResponseEntity update(@CurrentUser User currentUser,
                                  @PathVariable(value = "rSharedId") Long rSharedId) throws NotFoundException, RSharedAlreadyExistsException, InvalidDateRangeException {
        if (!currentUser.hasRole(Role.ERole.ROLE_ADMIN.toString()))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        RShared rShared = this.rSharedService.findOne(rSharedId);
        if (rShared.isActive()) return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
        rShared.setActive(true);
        rShared = this.rSharedService.save(rShared);
        return ResponseEntity.ok(rShared);
    }

}
