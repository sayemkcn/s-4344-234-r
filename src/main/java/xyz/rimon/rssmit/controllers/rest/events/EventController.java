package xyz.rimon.rssmit.controllers.rest.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import xyz.rimon.rssmit.commons.PageAttr;
import xyz.rimon.rssmit.domains.Event;
import xyz.rimon.rssmit.domains.Role;
import xyz.rimon.rssmit.domains.User;
import xyz.rimon.rssmit.domains.annotations.CurrentUser;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;
import xyz.rimon.rssmit.exceptions.nullpointers.NullEventException;
import xyz.rimon.rssmit.services.EventService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/events")
public class EventController {

    private final EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    // GET ALL EVENTS PAGINATED
    @GetMapping("")
    private ResponseEntity getEvents(@CurrentUser User currentUser,
                                     @RequestParam(value = "page", required = false, defaultValue = "0") Integer page) {
        Page<Event> events;
        if (currentUser.hasRole(Role.ERole.ROLE_DEVELOPER.toString()))
            events = this.eventService.getEventsByClient(currentUser.getClient(),page, PageAttr.PAGE_SIZE);
        else
            events = this.eventService.getEvents(currentUser, page, PageAttr.PAGE_SIZE);
        return ResponseEntity.ok(events);
    }


    @PostMapping("")
    private ResponseEntity postEvents(@RequestBody List<Event> eventList, BindingResult bindingResult,
                                       @CurrentUser User currentUser) throws NullEventException, UserNotFoundException {
        if (bindingResult.hasErrors())
            return ResponseEntity.badRequest().build();

        eventList = this.eventService.save(eventList, currentUser);
        return ResponseEntity.ok(eventList);
    }

}
