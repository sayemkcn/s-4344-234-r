package xyz.rimon.rssmit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import xyz.rimon.rssmit.commons.utils.PasswordUtil;
import xyz.rimon.rssmit.services.impl.CustomUserDetailsService;

@SpringBootApplication
@EnableGlobalMethodSecurity
public class RssmitApplication {

    @Autowired
    private CustomUserDetailsService userDetailsService;

    public static void main(String[] args) {
        SpringApplication.run(RssmitApplication.class, args);
    }

    @Autowired
    public void authenticationManager(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(userDetailsService).passwordEncoder(PasswordUtil.getBCryptPasswordEncoder());
    }

}
