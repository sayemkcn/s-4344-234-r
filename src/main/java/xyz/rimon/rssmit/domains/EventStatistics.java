package xyz.rimon.rssmit.domains;

public class EventStatistics {
    private long totalEvents;
    private long weightSum;

    public EventStatistics() {
    }

    public EventStatistics(Long totalEvents, Long weightSum) {
        if (totalEvents==null) totalEvents = 0L;
        if (weightSum==null) weightSum = 0L;
        this.totalEvents = totalEvents;
        this.weightSum = weightSum;
    }

    public long getTotalEvents() {
        return totalEvents;
    }

    public void setTotalEvents(long totalEvents) {
        this.totalEvents = totalEvents;
    }

    public long getWeightSum() {
        return weightSum;
    }

    public void setWeightSum(long weightSum) {
        this.weightSum = weightSum;
    }

    public long getActualWeightSum() {
        return this.totalEvents * 5;
    }

    @Override
    public String toString() {
        return "EventStatistics{" +
                "totalEvents=" + totalEvents +
                ", weightSum=" + weightSum +
                '}';
    }
}
