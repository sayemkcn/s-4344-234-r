package xyz.rimon.rssmit.domains;


import xyz.rimon.rssmit.domains.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class PaymentRequest extends BaseEntity {
    private String paymentMethod;
    private String accountNumber;
    private Integer requestAmount;
    private Boolean paid = false;

    @ManyToOne
    private User user;

    @OneToOne
    private Transactions trnx;

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Integer getRequestAmount() {
        return requestAmount;
    }

    public void setRequestAmount(Integer requestAmount) {
        this.requestAmount = requestAmount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Transactions getTrnx() {
        return trnx;
    }

    public void setTrnx(Transactions trnx) {
        this.trnx = trnx;
        this.paid = this.trnx != null;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }
}
