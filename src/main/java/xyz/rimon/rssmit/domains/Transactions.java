package xyz.rimon.rssmit.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import xyz.rimon.rssmit.commons.utils.DateUtil;
import xyz.rimon.rssmit.domains.base.BaseEntity;
import xyz.rimon.rssmit.exceptions.invalid.InvalidPaymentException;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Transactions extends BaseEntity {
    private Long balance;
    private Long debit;
    private Long credit;
    private String reference;
    private String explanation;
    private Date fromDate;
    private Date toDate;
    private boolean payment;
    private String trnxId;

    @ManyToOne
    @JsonIgnore
    private User user;

    @ManyToOne
    @JsonIgnore
    private RShared rShared;

    public Transactions() {
        this.balance = 0L;
        this.debit = 0L;
        this.credit = 0L;
        this.payment = false;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Long getDebit() {
        if (debit == null) return 0L;
        return debit;
    }

    public void setDebit(Long debit) {
        this.balance += debit;
        this.payment = false;
        this.debit = debit;
    }

    public Long getCredit() {
        if (credit == null) return 0L;
        return credit;
    }

    public void setCredit(Long credit) throws InvalidPaymentException {
        if (credit > this.balance) throw new InvalidPaymentException("Payment can\'t be greater than current balance!");
        this.balance -= credit;
        this.payment = true;
        this.credit = credit;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public RShared getrShared() {
        return rShared;
    }

    public void setrShared(RShared rShared) {
        this.rShared = rShared;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public boolean isPayment() {
        return payment;
    }

    public void setPayment(boolean payment) {
        this.payment = payment;
    }

    @JsonIgnore
    public boolean isInCurrentMonth() {
        return DateUtil.isInCurrentMonthYear(this.getCreated());
    }

    public String getTrnxId() {
        if (trnxId == null) return String.valueOf(this.getId());
        return trnxId;
    }

    public void setTrnxId(String trnxId) {
        this.trnxId = trnxId;
    }
}
