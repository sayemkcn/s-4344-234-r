package xyz.rimon.rssmit.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import xyz.rimon.rssmit.domains.base.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class RShared extends BaseEntity {
    @Column(nullable = false)
    @NotNull
    private Long revenueAmount;
    @Column(nullable = false)
    @NotNull
    private byte sharePercentage;
    private Long sharedAmount;
    private Long companyCharge;
    private boolean active;
    @Column(nullable = false)
    @NotNull
    private Date fromDate;
    @Column(nullable = false)
    @NotNull
    private Date toDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private User developer;

    @PrePersist
    @PreUpdate
    private void setSharedAmount() {
        this.sharedAmount = (this.revenueAmount * this.sharePercentage) / 100;
        if (this.revenueAmount < 20000)
            this.companyCharge = 0L;
        else
            this.companyCharge = 5 * this.sharedAmount / 100;
        this.sharedAmount -= this.companyCharge;
        if (this.getId() == null)
            this.developer = getLoggedinUser();
        // auto activate rshared if company charge is zero
        if (this.companyCharge == 0)
            this.active = true;
    }

    public String getDevName(){
        return this.developer.getName();
    }

    public Long getDevId(){
        return this.developer.getId();
    }

    public String getDevEmail(){
        return this.developer.getEmail();
    }

    public User getDeveloper() {
        return developer;
    }

    public void setDeveloper(User user) {
        this.developer = user;
    }

    public Long getRevenueAmount() {
        return revenueAmount;
    }

    public void setRevenueAmount(Long revenueAmount) {
        this.revenueAmount = revenueAmount;
    }

    public byte getSharePercentage() {
        return sharePercentage;
    }

    public void setSharePercentage(byte sharePercentage) {
        this.sharePercentage = sharePercentage;
    }

    public Long getSharedAmount() {
        return sharedAmount;
    }

    public void setSharedAmount(Long sharedAmount) {
        this.sharedAmount = sharedAmount;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Long getCompanyCharge() {
        return companyCharge;
    }

    public void setCompanyCharge(Long companyCharge) {
        this.companyCharge = companyCharge;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
