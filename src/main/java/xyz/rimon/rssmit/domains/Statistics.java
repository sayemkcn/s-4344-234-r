package xyz.rimon.rssmit.domains;

public class Statistics {
    private long numberOfEvents;
    private long weightSum;
    private long totalUsers;
    private long totalSharedAmount;
    private long totalLiability;
    private long totalNumberOfTrnx;
    private long totalPaymentRequests;

    public long getNumberOfEvents() {
        return numberOfEvents;
    }

    public void setNumberOfEvents(long numberOfEvents) {
        this.numberOfEvents = numberOfEvents;
    }

    public long getWeightSum() {
        return weightSum;
    }

    public void setWeightSum(long weightSum) {
        this.weightSum = weightSum;
    }

    public long getTotalUsers() {
        return totalUsers;
    }

    public void setTotalUsers(long totalUsers) {
        this.totalUsers = totalUsers;
    }

    public long getTotalSharedAmount() {
        return totalSharedAmount;
    }

    public void setTotalSharedAmount(long totalSharedAmount) {
        this.totalSharedAmount = totalSharedAmount;
    }

    public long getTotalLiability() {
        return totalLiability;
    }

    public void setTotalLiability(long totalLiability) {
        this.totalLiability = totalLiability;
    }

    public long getTotalNumberOfTrnx() {
        return totalNumberOfTrnx;
    }

    public void setTotalNumberOfTrnx(long totalNumberOfTrnx) {
        this.totalNumberOfTrnx = totalNumberOfTrnx;
    }

    public long getTotalPaymentRequests() {
        return totalPaymentRequests;
    }

    public void setTotalPaymentRequests(long totalPaymentRequests) {
        this.totalPaymentRequests = totalPaymentRequests;
    }
}
