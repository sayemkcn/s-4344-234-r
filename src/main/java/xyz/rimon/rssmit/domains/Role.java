package xyz.rimon.rssmit.domains;

import org.codehaus.jackson.annotate.JsonBackReference;
import xyz.rimon.rssmit.domains.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
public class Role extends BaseEntity {
    private String name;
    private String role;
    @ManyToMany(mappedBy = "roles")
    @JsonBackReference
    private List<User> users;

    public Role() {
    }

    public Role(ERole eRole) {
        this.name = eRole.getValue();
        this.role = eRole.toString();
    }

    public enum ERole {
        ROLE_USER("User"), ROLE_DEVELOPER("Developer"), ROLE_ADMIN("Admin");
        private String value;

        ERole(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static class StringRole {
        public static final String ROLE_USER = "ROLE_USER";
        public static final String ROLE_DEVELOPER = "ROLE_DEVELOPER";
        public static final String ROLE_ADMIN = "ROLE_ADMIN";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
