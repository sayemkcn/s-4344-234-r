package xyz.rimon.rssmit.domains.pojo;

import java.util.Date;

public class UserRev {
    private Long currentMonthInteractionPoints;
    private Long currentBalance;
    private Long lastPaymentAmount;
    private Long previousMonthIncome;
    private int thresholdLimit;
    private boolean canRequestForPayment;
    private Date from;
    private Date to;

    public UserRev() {
    }

    public Long getCurrentMonthInteractionPoints() {
        return currentMonthInteractionPoints;
    }

    public void setCurrentMonthInteractionPoints(Long currentMonthInteractionPoints) {
        this.currentMonthInteractionPoints = currentMonthInteractionPoints;
    }

    public Long getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Long currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Long getLastPaymentAmount() {
        if (this.lastPaymentAmount == null) return 0L;
        return lastPaymentAmount;
    }

    public void setLastPaymentAmount(Long lastPaymentAmount) {
        this.lastPaymentAmount = lastPaymentAmount;
    }

    public Long getPreviousMonthIncome() {
        return previousMonthIncome;
    }

    public void setPreviousMonthIncome(Long previousMonthIncome) {
        this.previousMonthIncome = previousMonthIncome;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public int getThresholdLimit() {
        return thresholdLimit;
    }

    public void setThresholdLimit(int thresholdLimit) {
        this.thresholdLimit = thresholdLimit;
    }

    public boolean isCanRequestForPayment() {
        return canRequestForPayment;
    }

    public void setCanRequestForPayment(boolean canRequestForPayment) {
        this.canRequestForPayment = canRequestForPayment;
    }
}
