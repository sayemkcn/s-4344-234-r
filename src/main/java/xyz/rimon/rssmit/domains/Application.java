package xyz.rimon.rssmit.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import xyz.rimon.rssmit.domains.base.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "app_info")
public class Application extends BaseEntity {
    private String name;
    @Column(unique = true)
    private String packageName;

    private Long devId;
    private String devName;
    private String devEmail;

    @ManyToOne
    private OAuth2Client client;

    public Application() {
    }

    public Application(String name, String packageName, OAuth2Client client) {
        this.name = name;
        this.packageName = packageName;
        this.client = client;
    }


    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @JsonIgnore
    public OAuth2Client getClient() {
        return client;
    }

    public void setClient(OAuth2Client client) {
        this.client = client;
    }

    public String getDevName() {
        return devName;
    }

    public void setDevName(String devName) {
        this.devName = devName;
    }

    public String getDevEmail() {
        return devEmail;
    }

    public void setDevEmail(String devEmail) {
        this.devEmail = devEmail;
    }

    public Long getDevId() {
        return devId;
    }

    public void setDevId(Long devId) {
        this.devId = devId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
