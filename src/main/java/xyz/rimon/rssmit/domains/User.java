package xyz.rimon.rssmit.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import xyz.rimon.rssmit.domains.base.BaseEntity;
import xyz.rimon.rssmit.domains.pojo.Address;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "mit_user", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"username", "email"})
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User extends BaseEntity implements UserDetails {
    @NotNull
    @NotEmpty
    private String name;

    @Column(unique = true, nullable = false)
    @NotNull
    @NotEmpty
    private String username;

    @Column(unique = true, nullable = false)
    @NotNull
    @Email
    private String email;

    @NotEmpty
    @NotNull
    @Size(min = 6, max = 100, message = "Password must be between 6 to 100 characters!")
    @JsonIgnore
    @Column(length = 512, nullable = false)
    private String password;

    @Embedded
    private Address address;

    private Long currentBalance;

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Role> roles;
    private boolean enabled = false;
    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;


    @ManyToOne
    @JsonIgnore
    private OAuth2Client client;

    @PrePersist
    private void onPrePersist() {
        this.currentBalance = 0L;
        if (roles == null || roles.isEmpty())
            grantRole(new Role(Role.ERole.ROLE_USER));

    }

    @JsonIgnore
    public boolean isOnlyUser() {
        return this.roles.size() == 1 && hasRole(Role.ERole.ROLE_USER.toString());
    }

    public void grantRole(Role role) {
        if (this.roles == null)
            this.roles = new ArrayList<>();
        // check if user already has that role
        if (!hasRole(role.getRole()))
            this.roles.add(role);
    }

    public boolean hasRole(String role) {
        return this.roles != null && this.roles.stream()
                .filter(r -> r.getRole().trim().equals(role.trim()))
                .count() > 0;
    }

    @JsonIgnore
    public boolean isAdmin(){
        return this.hasRole(Role.ERole.ROLE_ADMIN.getValue());
    }

    @JsonIgnore
    public boolean hasSameClient(User user) {
        return this.client != null && user.getClient() != null
                && this.client.getClientId().equals(user.getClient().getClientId());
    }

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorityList = new ArrayList<>();
        for (Role role : this.roles) {
            GrantedAuthority authority = new SimpleGrantedAuthority(role.getRole());
            authorityList.add(authority);
        }
        return authorityList;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public String getActualEmail() {
        if (isOnlyUser()) {
            String[] eArr = this.email.split("__");
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < eArr.length; i++) {
                if (i == 0) continue;
                stringBuilder.append(eArr[i]);
            }
            return stringBuilder.toString();
        }
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public OAuth2Client getClient() {
        return client;
    }

    public void setClient(OAuth2Client client) {
        this.client = client;
    }

    public Long getCurrentBalance() {
        if (currentBalance == null) return 0L;
        return currentBalance;
    }

    public void setCurrentBalance(Long currentBalance) {
        this.currentBalance = currentBalance;
    }
}
