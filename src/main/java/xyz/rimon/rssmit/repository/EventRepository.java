package xyz.rimon.rssmit.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import xyz.rimon.rssmit.domains.Event;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.User;

import java.util.Date;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event,Long>{
    Page<Event> findByUser(User user, Pageable pageable);
    Page<Event> findByUserClient(OAuth2Client client, Pageable pageable);
    Long countByUserClientAndDateBetween(OAuth2Client client, Date fromDate,Date toDate);
    @Query("SELECT SUM (e.weight) FROM Event e WHERE e.user.client.id=:clientId AND e.date BETWEEN :fromDate AND :toDate")
    Long sumWeightByUserClientAndDateBetween(@Param("clientId") Long clientId,@Param("fromDate") Date fromDate,@Param("toDate") Date toDate);
    @Query("SELECT SUM (e.weight) FROM Event e WHERE e.user.id=:userId AND e.date BETWEEN :fromDate AND :toDate")
    Long sumWeightByUserAndDateBetween(@Param("userId") Long userId,@Param("fromDate") Date fromDate,@Param("toDate") Date toDate);

}
