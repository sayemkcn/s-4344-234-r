package xyz.rimon.rssmit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.User;

@Repository
public interface ClientRepository extends JpaRepository<OAuth2Client,Long>{
    OAuth2Client findByUserListIn(User user);
    OAuth2Client findByClientIdAndClientSecret(String clientId,String clientSecret);
    OAuth2Client findByClientId(String clientId);
}
