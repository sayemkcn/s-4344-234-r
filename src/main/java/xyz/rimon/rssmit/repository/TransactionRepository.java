package xyz.rimon.rssmit.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.RShared;
import xyz.rimon.rssmit.domains.Transactions;
import xyz.rimon.rssmit.domains.User;

import java.util.Date;
import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transactions, Long> {
    Transactions findFirstByUserOrderByIdDesc(User user);
    Page<Transactions> findByUser(User user, Pageable pageable);
    Page<Transactions> findByUserClient(OAuth2Client client, Pageable pageable);

    List<Transactions> findByrSharedAndPaymentTrue(RShared rShared);
    List<Transactions> findByrSharedAndPaymentFalse(RShared rShared);

    Transactions findFirstByUserIdAndPaymentTrueOrderByIdDesc(Long userId);

    Long countByUserClient(OAuth2Client client);
    Integer countByUserIdAndCreatedBetweenAndPaymentTrue(Long userId,Date fromDate,Date toDate);
}
