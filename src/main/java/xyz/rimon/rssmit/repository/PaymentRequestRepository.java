package xyz.rimon.rssmit.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.PaymentRequest;

import java.util.Date;

@Repository
public interface PaymentRequestRepository extends JpaRepository<PaymentRequest, Long> {
    Integer countByUserIdAndCreatedBetween(Long userId, Date from, Date to);
    Page<PaymentRequest> findByUserId(Long userId, Pageable pageable);
    Page<PaymentRequest> findByUserClientAndPaidTrue(OAuth2Client client,Pageable pageable);
    Page<PaymentRequest> findByUserClientAndPaidFalse(OAuth2Client client,Pageable pageable);

    Long countByUserClient(OAuth2Client client);
}
