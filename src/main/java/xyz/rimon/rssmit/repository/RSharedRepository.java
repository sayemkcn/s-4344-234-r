package xyz.rimon.rssmit.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.RShared;
import xyz.rimon.rssmit.domains.User;

import java.util.Date;

@Repository
public interface RSharedRepository extends JpaRepository<RShared, Long> {
    RShared findFirstByDeveloperClientOrderByIdDesc(OAuth2Client client);

    RShared findByDeveloperAndFromDateOrToDate(User user, Date fromDate, Date toDate);

    RShared findByFromDateBetween(Date fromDate, Date toDate);

    RShared findByToDateBetween(Date fromDate, Date toDate);

    RShared findByDeveloperAndFromDateBetween(User dev,Date fromDate,Date toDate);
    RShared findByDeveloperAndToDateBetween(User dev,Date fromDate,Date toDate);

    RShared findByIdAndDeveloper(Long rsharedId, User developer);

    Page<RShared> findAllByDeveloper(User dev, Pageable pageable);

    Page<RShared> findByActiveFalse(Pageable pageable);

    Page<RShared> findByActiveTrue(Pageable pageable);

    @Query("SELECT SUM(r.sharedAmount) FROM RShared r WHERE r.developer.client.id=:clientId")
    Long sumOfAllSharedAmounts(@Param("clientId") Long clientId);
}
