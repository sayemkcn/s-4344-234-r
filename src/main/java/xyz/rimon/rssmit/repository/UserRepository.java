package xyz.rimon.rssmit.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import xyz.rimon.rssmit.domains.OAuth2Client;
import xyz.rimon.rssmit.domains.Role;
import xyz.rimon.rssmit.domains.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Long>{
    User findByUsername(String username);
    User findByEmail(String email);
    User findByClientAndRolesIn(OAuth2Client client, Role role);

    Page<User> findByRolesIn(Role role,Pageable pageable);

    @Query("SELECT u.id FROM User u WHERE u.client.clientId = :clientId")
    List<Long> findIdsByClient(@Param("clientId") String clientId);

    Page<User> findByClient(OAuth2Client client, Pageable pageable);

    Page<User> findByClientClientId(String clientId, Pageable pageable);

    Page<User> findByIdIn(List<Long> ids, Pageable pageable);

    Page<User> findByClientAndIdNotInAndCurrentBalanceGreaterThanEqual(OAuth2Client client,List<Long> ids, Long minBalance, Pageable pageable);

    Page<User> findByClientAndIdIn(OAuth2Client client, List<Long> ids, Pageable pageable);

    Long countAllByClient(OAuth2Client client);

    @Query("SELECT SUM(u.currentBalance) FROM User u WHERE u.client.id=:clientId")
    Long sumOfAllUsersBalance(@Param("clientId") Long clientId);
}
