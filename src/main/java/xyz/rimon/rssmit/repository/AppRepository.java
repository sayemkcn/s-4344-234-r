package xyz.rimon.rssmit.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.rimon.rssmit.domains.Application;

@Repository
public interface AppRepository extends JpaRepository<Application, Long> {
    Application findByPackageName(String packageName);
    Page<Application> findByClientId(Long clientId, Pageable pageable);
}
