package xyz.rimon.rssmit.exceptions.exists;

public class RSharedAlreadyExistsException extends AlreadyExistsException{
    public RSharedAlreadyExistsException() {
    }

    public RSharedAlreadyExistsException(String message) {
        super(message);
    }

    public RSharedAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public RSharedAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public RSharedAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
