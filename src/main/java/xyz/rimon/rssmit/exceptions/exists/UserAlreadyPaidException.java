package xyz.rimon.rssmit.exceptions.exists;

public class UserAlreadyPaidException extends AlreadyExistsException{
    public UserAlreadyPaidException() {
    }

    public UserAlreadyPaidException(String message) {
        super(message);
    }

    public UserAlreadyPaidException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserAlreadyPaidException(Throwable cause) {
        super(cause);
    }

    public UserAlreadyPaidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
