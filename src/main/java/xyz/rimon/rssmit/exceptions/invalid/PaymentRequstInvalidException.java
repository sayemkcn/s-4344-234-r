package xyz.rimon.rssmit.exceptions.invalid;

public class PaymentRequstInvalidException extends InvalidException{
    public PaymentRequstInvalidException() {
    }

    public PaymentRequstInvalidException(String message) {
        super(message);
    }

    public PaymentRequstInvalidException(String message, Throwable cause) {
        super(message, cause);
    }

    public PaymentRequstInvalidException(Throwable cause) {
        super(cause);
    }

    public PaymentRequstInvalidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
