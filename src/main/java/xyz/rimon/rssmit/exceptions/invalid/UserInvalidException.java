package xyz.rimon.rssmit.exceptions.invalid;

public class UserInvalidException extends InvalidException{
    public UserInvalidException() {
    }

    public UserInvalidException(String message) {
        super(message);
    }

    public UserInvalidException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserInvalidException(Throwable cause) {
        super(cause);
    }

    public UserInvalidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
