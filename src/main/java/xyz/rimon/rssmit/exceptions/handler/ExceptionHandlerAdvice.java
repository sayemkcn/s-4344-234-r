package xyz.rimon.rssmit.exceptions.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import xyz.rimon.rssmit.exceptions.exists.RSharedAlreadyExistsException;
import xyz.rimon.rssmit.exceptions.exists.UserAlreadyExistsException;
import xyz.rimon.rssmit.exceptions.exists.UserAlreadyPaidException;
import xyz.rimon.rssmit.exceptions.invalid.InvalidDateRangeException;
import xyz.rimon.rssmit.exceptions.invalid.InvalidPaymentException;
import xyz.rimon.rssmit.exceptions.invalid.PaymentRequstInvalidException;
import xyz.rimon.rssmit.exceptions.notfound.ClientNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.PaymentRequestNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.RSharedNotFoundException;
import xyz.rimon.rssmit.exceptions.notfound.UserNotFoundException;

@ControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler(ClientNotFoundException.class)
    private ResponseEntity handleClientNotFoundException(ClientNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(ex.getMessage());
    }

    @ExceptionHandler(UserNotFoundException.class)
    private ResponseEntity handleUserNotFoundException(UserNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(ex.getMessage());
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    private ResponseEntity handleUserAlreadyExistsException(UserAlreadyExistsException ex) {
        return ResponseEntity.status(HttpStatus.IM_USED).body(ex.getMessage());
    }

    @ExceptionHandler(RSharedAlreadyExistsException.class)
    private ResponseEntity handleMultipleRevenueSharingInASameDateRange(RSharedAlreadyExistsException ex) {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ex.getMessage());
    }

    @ExceptionHandler(RSharedNotFoundException.class)
    private ResponseEntity handleRSharedNotFoundException(RSharedNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(ex);
    }

    @ExceptionHandler(InvalidDateRangeException.class)
    private ResponseEntity handleInvalidDateRange(InvalidDateRangeException ex) {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ex.getMessage());
    }

    @ExceptionHandler(UserAlreadyPaidException.class)
    private ResponseEntity handleUserAlreadyPaidException(UserAlreadyPaidException ex) {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ex.getMessage());
    }

    @ExceptionHandler(PaymentRequstInvalidException.class)
    private ResponseEntity handlePaymentRequstInvalidException(PaymentRequstInvalidException ex) {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ex.getMessage());
    }


    @ExceptionHandler(PaymentRequestNotFoundException.class)
    private ResponseEntity handlePaymentRequestNotFoundException(PaymentRequestNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ex.getMessage());
    }

    @ExceptionHandler(InvalidPaymentException.class)
    private ResponseEntity handleInvalidPaymentException(InvalidPaymentException ex) {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(ex.getMessage());
    }
}
