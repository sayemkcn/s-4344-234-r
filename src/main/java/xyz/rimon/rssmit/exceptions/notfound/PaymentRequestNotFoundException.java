package xyz.rimon.rssmit.exceptions.notfound;

public class PaymentRequestNotFoundException extends NotFoundException {
    public PaymentRequestNotFoundException() {
    }

    public PaymentRequestNotFoundException(String message) {
        super(message);
    }

    public PaymentRequestNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PaymentRequestNotFoundException(Throwable cause) {
        super(cause);
    }

    public PaymentRequestNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
