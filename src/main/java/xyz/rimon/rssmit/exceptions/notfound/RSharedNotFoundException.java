package xyz.rimon.rssmit.exceptions.notfound;

public class RSharedNotFoundException extends NotFoundException{
    public RSharedNotFoundException() {
    }

    public RSharedNotFoundException(String message) {
        super(message);
    }

    public RSharedNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RSharedNotFoundException(Throwable cause) {
        super(cause);
    }

    public RSharedNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
