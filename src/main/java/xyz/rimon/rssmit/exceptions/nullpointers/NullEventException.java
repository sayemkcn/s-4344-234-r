package xyz.rimon.rssmit.exceptions.nullpointers;

public class NullEventException extends Throwable{
    public NullEventException() {
    }

    public NullEventException(String message) {
        super(message);
    }

    public NullEventException(String message, Throwable cause) {
        super(message, cause);
    }

    public NullEventException(Throwable cause) {
        super(cause);
    }

    public NullEventException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
